﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Centralized listener for the input
/// </summary>
public class InputManager : MonoBehaviour
{
    public static Action<Vector2> OnSwipe;
    public static Action<Vector2> OnDrag;
    public static Action<Vector2> OnMouseUp;
    public static Action<Vector2> OnMouseDown;
    public static Action OnZoomIn;
    public static Action OnZoomOut;

    public static Vector3 MousePosition => Input.mousePosition;

    public static bool BlockAllInput;

    [SerializeField]
    private float _swipeThresholdPC = 0.5f;
    [SerializeField]
    private float _swipeThresholdMobile = 70f;

    private bool _wasZoomingLastFrame;
    private Vector2[] _lastZoomPositions;

    private void Awake()
    {
        BlockAllInput = false;
    }

    private void Update()
    {
        if (BlockAllInput)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            OnMouseDown?.Invoke(Input.mousePosition);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            OnMouseUp?.Invoke(Input.mousePosition);
        }

        //Zoom input has priority over drag
        /*********** Handle Zoom ***********/
        if (Input.GetAxis("Mouse ScrollWheel") > 0) // forward
        {
            OnZoomIn?.Invoke();
            return;
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0) // back            
        {
            OnZoomOut?.Invoke();
            return;
        }

        //Mobile Zoom
        if (Input.touchCount == 2)
        {
            Vector2[] newPositions = new Vector2[] { Input.GetTouch(0).position, Input.GetTouch(1).position };
            if (!_wasZoomingLastFrame)
            {
                _lastZoomPositions = newPositions;
                _wasZoomingLastFrame = true;
            }
            else
            {
                // Zoom based on the distance between the new positions compared to the 
                // distance between the previous positions.
                float newDistance = Vector2.Distance(newPositions[0], newPositions[1]);
                float oldDistance = Vector2.Distance(_lastZoomPositions[0], _lastZoomPositions[1]);

                if (newDistance > oldDistance)
                {
                    OnZoomIn?.Invoke();
                    return;
                }
                else if (newDistance < oldDistance)
                {
                    OnZoomOut?.Invoke();
                    return;
                }

                _lastZoomPositions = newPositions;
            }
        }
        else
        {
            _wasZoomingLastFrame = false;
        }

        /*********** Handle Drag ***********/
        Vector2 delta;
        float threshold;

#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            float x = Input.GetAxis("Mouse X");
            float y = Input.GetAxis("Mouse Y");
            delta = new Vector2(x, y);
            threshold = _swipeThresholdPC;
#else
        if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved)
        {
            delta = Input.touches[0].deltaPosition;
            threshold = _swipeThresholdMobile;
#endif

            if (delta.magnitude > threshold)
            {
                OnSwipe?.Invoke(delta);
            }

            OnDrag?.Invoke(delta);
        }        
    }
}