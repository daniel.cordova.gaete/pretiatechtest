﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Confirmation popup used by the HUD
/// </summary>
public class ConfirmationPopup : MonoBehaviour
{
    [SerializeField]
    private Text _message;
    [SerializeField]
    private Button _confirmBtn;

    public void ShowPopup(bool show, string msg = "", UnityEngine.Events.UnityAction onConfirm = null)
    {
        gameObject.SetActive(show);

        if(show)
        {
            _message.text = msg;

            if (onConfirm != null)
                _confirmBtn.onClick.AddListener(onConfirm);
        }
        else
        {
            //clear possible callbacks
            if (onConfirm != null)
                _confirmBtn.onClick.RemoveAllListeners();
        }
    }

    public void ClosePopup()
    {
        ShowPopup(false);
    }
}
