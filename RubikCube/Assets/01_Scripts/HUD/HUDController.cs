﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
/// Controller for the gameplay HUD
/// </summary>
public class HUDController : MonoBehaviour
{
    public static HUDController Instance { get; private set; }

    public static readonly string c_timerData = "time";
    public static float s_startingTime = 0f;

    [SerializeField]
    private Text _timerLbl;
    [SerializeField]
    private GameObject _settingsPopup;    
    [SerializeField]
    private ConfirmationPopup _confirmPopup;
    [SerializeField]
    private ConfirmationPopup _finishPopup;

    public bool BlockHUD { get; set; }

    private float _timePassed, _previousTime;
    private bool _playing;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        _playing = false;
        //_timePassed = s_startingTime;

        Time.timeScale = 1f;
    }

    private void Update()
    {
        if (!_playing)
            return;

        _timePassed += Time.deltaTime;

        //only change label when the seconds increase
        if((int)_timePassed != (int)_previousTime)
        {
            //save timer
            SaveHandler.SaveFloat(c_timerData, _timePassed);
            _timerLbl.text = GetFormattedTime();
        }
    }

    public void ShowMenu(bool show)
    {
        if (BlockHUD)
            return;

        _settingsPopup.SetActive(show);

        //pause game
        if (_playing)
            Time.timeScale = show ? 0f : 1f;
    }

    public void StartTimer()
    {
        _timePassed = s_startingTime;
        _previousTime = 0f;

        _playing = true;
    }

    private string GetFormattedTime()
    {
        TimeSpan time = TimeSpan.FromSeconds(_timePassed);
        return time.ToString(@"mm\:ss");
    }

    public void FinishGame()
    {
        Time.timeScale = 0f;
        _playing = false;

        //reset the cube's state
        RubikHandler.RestartLevel();

        StartCoroutine(WaitForCameraAndFinish());
    }

    private IEnumerator WaitForCameraAndFinish()
    {
        InputManager.BlockAllInput = true;
        Time.timeScale = 0f;

        yield return CameraController.Instance.PlayFinishAnimation();

        //TODO: let the player move the camera after finishing
        _finishPopup.ShowPopup(true, $"You Win! Your time was {GetFormattedTime()}");

        InputManager.BlockAllInput = false;
    }

    public void ShowTimer(bool show)
    {
        _timerLbl.gameObject.SetActive(show);
    }

    public void TryToRestart()
    {
        _confirmPopup.ShowPopup(true, "Are You sure you want to restart? All your progress will be lost", Restart);
    }

    public void TryToMainMenu()
    {
        _confirmPopup.ShowPopup(true, "Are You sure you want to quit?", MainMenu);
    }

    private void Restart()
    {
        ShowMenu(false);

        RubikHandler.RestartLevel();

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void MainMenu()
    {
        ShowMenu(false);
        SceneManager.LoadScene("Start");
    }
}
