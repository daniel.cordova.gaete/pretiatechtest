﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Save and Load using PlayerPrefs
/// </summary>
public class SaveHandler
{
#if UNITY_EDITOR
    [UnityEditor.MenuItem("Save Data/Delete Save")]
#endif
    public static void DeleteSave()
    {
        PlayerPrefs.DeleteAll();
    }

    /// <summary>
    /// Reset game specific data
    /// </summary>
    public static void RestartGameData(int desiredCubeSize)
    {
        //clear all
        DeleteSave();
        LoadGame();

        //recover size
        RubikHandler.s_sizeSelected = desiredCubeSize;
        SaveInt(RubikHandler.c_sizeData, desiredCubeSize);
    }

    /// <summary>
    /// Loads the specific game data
    /// </summary>
    /// <returns>If there was a save found</returns>
    public static bool LoadGame()
    {
        //load data
        RubikHandler.s_sizeSelected = LoadInt(RubikHandler.c_sizeData);        
        RubikHandler.s_savedRandom = LoadArray<MoveInfo>(RubikHandler.c_randomSavedData);
        RubikHandler.s_savedHistory = LoadArray<MoveInfo>(RubikHandler.c_historySavedData);

        HUDController.s_startingTime = LoadFloat(HUDController.c_timerData);

        return HUDController.s_startingTime > 0f;
    }

    #region Operations
    public static void SaveInt(string name, int field)
    {
        PlayerPrefs.SetInt(name, field);
        PlayerPrefs.Save();
    }

    private static int LoadInt(string name)
    {
        return PlayerPrefs.GetInt(name, 0);
    }

    public static void SaveFloat(string name, float field)
    {
        PlayerPrefs.SetFloat(name, field);
        PlayerPrefs.Save();
    }

    private static float LoadFloat(string name)
    {
        return PlayerPrefs.GetFloat(name, 0f);
    }

    public static void SaveArray<T>(string name, T[] array)
    {
        string json = JsonHelper.ToJson<T>(array);

        PlayerPrefs.SetString(name, json);
        PlayerPrefs.Save();
    }

    private static T[] LoadArray<T>(string name)
    {
        string json = PlayerPrefs.GetString(name, "{}");

        var result = JsonHelper.FromJson<T>(json);

        return result != null ? result : new T[0];
    }
    #endregion Operations
}
