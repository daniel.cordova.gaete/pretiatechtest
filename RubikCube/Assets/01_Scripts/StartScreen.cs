﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Controller for the game's first screen
/// </summary>
public class StartScreen : MonoBehaviour
{
    private int _selectedSize;

    [SerializeField]
    private Button _continueBtn;
    [SerializeField]
    private Toggle _defaultSizeOption;

    private void Start()
    {
        //target fps
        Application.targetFrameRate = 60;

        //continue button should be disabled if no save is found
        _continueBtn.interactable = SaveHandler.LoadGame();

        //this is a workaround for a bug present in this version of Unity 
        //https://issuetracker.unity3d.com/issues/re-enabling-game-object-with-toggle-group-loses-information-about-previously-checked-toggle?_ga=2.44222628.1823124199.1578773442-615389317.1578773442
        _defaultSizeOption.onValueChanged.Invoke(true);
    }

    public void StartGame(bool newGame)
    {
        if (newGame)
        {
            SaveHandler.RestartGameData(_selectedSize);
        }

        SceneManager.LoadScene("Gameplay", LoadSceneMode.Single);
    }

    public void SetSize(int size)
    {
        /*
         * This method is linked to the toggle's events and thus gets called twice, once when deselecting
         * and once when selecting the new one. This means that the last value set will be the one we want.
         * Nevertheless it has an odd behavior when you start the game without touching any toggle, so we need
         * to set a default value in the Start() method
         */
        _selectedSize = size;
    }
}
