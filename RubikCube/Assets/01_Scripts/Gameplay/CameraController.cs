﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance { get; private set; }

    [SerializeField]
    private Transform _target;
    [SerializeField]
    private float _rotSpeed = 80f;
    [SerializeField, Tooltip("Lower and Upper angle for the camera to tilt vertically")]
    private Vector2 _cameraVerticalLimits;

    [Header("Zoom")]
    [SerializeField]
    private float _zoomSpeed = 10f;
    [SerializeField]
    private Vector2 _zoomLimits;

    [Header("Finish Animation")]
    [SerializeField]
    private float _zoomAnimLimit = 25f;
    [SerializeField]
    private float _zoomOutTime = 2f;
    [SerializeField, Tooltip("Time to spin around the cube")]
    private float _spinTime = 4f;
    [SerializeField, Tooltip("SpinSpeed")]
    private float _spinSpeed = 100f;

    public bool BlockCamera { get; set; }

    private Vector2 _initMousePos;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void Start()
    {
        InputManager.OnMouseDown += StartDrag;
        InputManager.OnDrag += DragCamera;
        InputManager.OnZoomIn += OnZoomIn;
        InputManager.OnZoomOut += OnZoomOut;

        //smooth move to compensate for scene camera orientation
        transform.DOLookAt(_target.position, 1f);
    }

    #region Events
    private void StartDrag(Vector2 mousePos)
    {
        _initMousePos = mousePos;
    }

    private void DragCamera(Vector2 delta)
    {
        if (BlockCamera)
            return;

        //use screen input positions to simplify the direction detection
        Vector2 differenceWithMouseDown = new Vector2(InputManager.MousePosition.x - _initMousePos.x,
                                                        InputManager.MousePosition.y - _initMousePos.y);

        ApplyRotation(differenceWithMouseDown * Time.deltaTime);
    }

    private void OnZoomIn()
    {
        if (BlockCamera)
            return;

        ApplyZoom(_zoomSpeed * Time.deltaTime);
    }

    private void OnZoomOut()
    {
        if (BlockCamera)
            return;

        ApplyZoom(-_zoomSpeed * Time.deltaTime);
    }
    #endregion Events

    private void ApplyRotation(Vector2 movementDelta)
    {
        float rotX = movementDelta.x * _rotSpeed;
        float rotY = movementDelta.y * _rotSpeed;

        //rotate around the world Up vector and around the camera Right vector
        transform.RotateAround(_target.position, Vector3.up, rotX);

        //protect upper and lower camera limits
        float vertAngle = Vector3.Angle(transform.forward, Vector3.up);

        if (rotY > 0f && vertAngle > _cameraVerticalLimits.x || //if can move down
            rotY < 0f && vertAngle < _cameraVerticalLimits.y) //if can move up
            transform.RotateAround(_target.position, transform.right, -rotY);

        transform.LookAt(_target);
    }

    private void ApplyZoom(float delta)
    {
        Vector3 newPos = transform.position + transform.forward * delta;

        float newDistance = Vector3.Distance(newPos, _target.position);

        if (newDistance < _zoomLimits.x || newDistance > _zoomLimits.y)
            return;

        transform.position += transform.forward * delta;
    }

    private void OnDestroy()
    {
        InputManager.OnMouseDown -= StartDrag;
        InputManager.OnDrag -= DragCamera;
        InputManager.OnZoomIn -= OnZoomIn;
        InputManager.OnZoomOut -= OnZoomOut;
    }

    public IEnumerator PlayFinishAnimation()
    {
        HUDController.Instance.BlockHUD = true;
        BlockCamera = true;

        //Zoom Out emulating the pinch behavior
        Vector3 finalPos = (transform.position - _target.position).normalized * _zoomAnimLimit;
        yield return transform.DOMove(finalPos, _zoomOutTime)
                                .SetUpdate(true)
                                .WaitForCompletion();

        //Spin emulating the drag behavior        
        //we will emulate the movement by feeding the system with a vector similar to a drag
        //with certain speed and deltaTime at 30fps
        Vector2 simulatedDrag = _spinSpeed * Vector2.right * 0.033f; 

        float rotationCounter = 0f;
        float targetRot = 360f * _spinSpeed;

        yield return DOTween.To(() => rotationCounter, (x) => rotationCounter = x, targetRot, _spinTime)
                            .OnUpdate(() => ApplyRotation(simulatedDrag))
                            .SetUpdate(true)
                            .WaitForCompletion();

        //a little delay at the end
        yield return new WaitForSecondsRealtime(1f);

        HUDController.Instance.BlockHUD = false;
        BlockCamera = false;
    }
}
