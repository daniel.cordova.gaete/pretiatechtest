﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// Controller for the Cube
/// </summary>
public class RubikHandler : MonoBehaviour
{
    #region Save Data
    public static readonly string c_sizeData = "size";
    public static readonly string c_randomSavedData = "random";
    public static readonly string c_historySavedData = "history";

    public static int s_sizeSelected = 3;
    public static MoveInfo[] s_savedRandom;
    public static MoveInfo[] s_savedHistory;
    #endregion Save Data

    /// <summary>
    /// A Face can be defined by an Axis and the coordinate for said axis
    /// </summary>
    private struct FaceInfo
    {
        public MoveInfo.Axis Axis;
        public float AxisCoord;
    }

    public static readonly float c_rotationDegrees = 90f;

    [SerializeField]
    private Cubie _cubiePrefab;
    [SerializeField]
    private int _randomMovements = 5;
    [SerializeField]
    private float _faceRotationTime = 0.5f;

    /// <summary>
    /// All the Cubies instantiated
    /// </summary>
    private List<Cubie> _cubies = new List<Cubie>();

    #region Axis Handling
    /// <summary>
    /// Separation between Cubies, must match their size
    /// </summary>
    private readonly float c_axisDistance = 1f;
    /// <summary>
    /// Points where the Cubies where instantiated
    /// </summary>
    /// <remarks>Since the figure is symmetrical we can use this in any axis</remarks>
    private List<float> _validCoordinates = new List<float>();
    /// <summary>
    /// List of cubies for each face
    /// </summary>
    private Dictionary<FaceInfo, List<Cubie>> _facesInitialState = new Dictionary<FaceInfo, List<Cubie>>();
    #endregion Axis Handling

    private Cubie _selectedCubie = null;

    private List<MoveInfo> _movesHistory = new List<MoveInfo>();

    private bool _ready;

    #region Initialization
    private IEnumerator Start()
    {
        CameraController.Instance.BlockCamera = true;
        _ready = false;

        InputManager.OnSwipe += TryToRotate;
        InputManager.OnMouseUp += UnblockCamera;

        //instantiate objects
        Instantiation();

        //History and random movements
        yield return InitialMovements();

        //Notify the HUD
        HUDController.Instance.StartTimer();

        CameraController.Instance.BlockCamera = false;
        _ready = true;
    }

    private void Instantiation()
    {
        //With the Cube centered at (0,0,0) we can calculate the dimensions according to the amount of Cubies per side
        float axisCoordinate = (s_sizeSelected - 1) / 2f;
        float axisCoordinateABS = Mathf.Abs(axisCoordinate);

        int id = 0;
        //with this axis representation each face is described by a constant plane (x=0, y=1, etc)
        for (float x = -axisCoordinate; x <= axisCoordinate; x += c_axisDistance)
        {
            _validCoordinates.Add(x);

            for (float y = -axisCoordinate; y <= axisCoordinate; y += c_axisDistance)
            {
                for (float z = -axisCoordinate; z <= axisCoordinate; z += c_axisDistance)
                {
                    //TODO: verify condition to don't spawn inside the cube
                    //if (x == 0 && y == 0 && z == 0)
                    //    continue;

                    var c = Instantiate<Cubie>(_cubiePrefab, transform);
                    c.Init(this, new Vector3(x, y, z), id);

                    _cubies.Add(c);

                    //add to planes if it belongs to the exterior Cubies (+/-axisCoordinate)
                    if (Mathf.Abs(x) == axisCoordinateABS)
                    {
                        AddCubieToFace(MoveInfo.Axis.X, x, c);
                    }
                    if (Mathf.Abs(y) == axisCoordinateABS)
                    {
                        AddCubieToFace(MoveInfo.Axis.Y, y, c);
                    }
                    if (Mathf.Abs(z) == axisCoordinateABS)
                    {
                        AddCubieToFace(MoveInfo.Axis.Z, z, c);
                    }

                    ++id;
                }
            }
        }
    }

    private IEnumerator InitialMovements()
    {
        //this is needed when testing the gameplay scene directly
        if (s_savedHistory == null)
        {
            s_savedHistory = new MoveInfo[0];
            s_savedRandom = new MoveInfo[0];
        }

        _movesHistory.AddRange(s_savedHistory);

        //Apply Saved Data
        if (s_savedRandom.Length > 0)
        {
            foreach (MoveInfo m in s_savedRandom)
                Rotate(m, 0f);

            foreach (MoveInfo m in _movesHistory)
                Rotate(m, 0f);
        }
        else
        {
            //little delay before scramble
            yield return new WaitForSeconds(1f);

            //initial random movements
            List<MoveInfo> randomMoves = new List<MoveInfo>();
            MoveInfo lastMove = new MoveInfo(MoveInfo.Axis.X, 0f, 0f);
            for (int i = 0; i < _randomMovements; ++i)
            {
                MoveInfo rand = GetRandomMove(lastMove);
                randomMoves.Add(rand);
                Rotate(rand, _faceRotationTime, false);

                //delay between rotations
                yield return new WaitForSeconds(2 * _faceRotationTime);

                lastMove = rand;
            }
            //if the random accidentally completes the game make one more move
            if (IsCubeCompleted())
            {
                MoveInfo rand = GetRandomMove(lastMove);
                randomMoves.Add(rand);
                Rotate(rand, _faceRotationTime, false);

                yield return new WaitForSeconds(2 * _faceRotationTime);
            }

            SaveHandler.SaveArray<MoveInfo>(c_randomSavedData, randomMoves.ToArray());
        }

        yield return new WaitForSeconds(1f);
    }

    #endregion Initialization

    private void AddCubieToFace(MoveInfo.Axis a, float coord, Cubie c)
    {
        FaceInfo f = new FaceInfo()
        {
            Axis = a,
            AxisCoord = coord
        };

        if (!_facesInitialState.ContainsKey(f))
            _facesInitialState.Add(f, new List<Cubie>());

        _facesInitialState[f].Add(c);
    }

    private void TryToRotate(Vector2 swipeDirection)
    {
        if (_selectedCubie == null || !_ready)
            return;

        //find the axis this cubie belongs to
        MoveInfo move = MoveInfo.CalculateMove(_selectedCubie.transform);
        _movesHistory.Add(move);
        Rotate(move, _faceRotationTime);

        _selectedCubie = null;        

        SaveHandler.SaveArray<MoveInfo>(c_historySavedData, _movesHistory.ToArray());
    }

    private void UnblockCamera(Vector2 inputPos)
    {
        if(_selectedCubie == null)
            CameraController.Instance.BlockCamera = false;
    }

    private void Rotate(MoveInfo move, float rotationTime, bool checkCompletion = true)
    {
        //ignore input if an animation is playing
        InputManager.BlockAllInput = true;

        var face = _cubies.Select(x => x.transform)
                        .Where(move.TransformBelongsToSelectedPlane());

        //instant movement, meant to recover save state and similar
        if (rotationTime == 0f)
        {
            foreach (var c in face)
            {
                c.RotateAround(Vector3.zero, move.GetRotationAxis(), move.Degrees);
            }

            OnRotationComplete(checkCompletion);
        }
        else
        {
            //animate rotation, we use RotateAround() with a step that depends of Fixed Delta Time
            float rotCounter = 0f;
            DOTween.To(() => rotCounter, (x) => rotCounter = x, move.Degrees, _faceRotationTime)
                    .SetUpdate(UpdateType.Fixed)
                    .OnUpdate(() =>
                    {
                        foreach (var c in face)
                        {
                            c.RotateAround(Vector3.zero, move.GetRotationAxis(), Time.fixedDeltaTime * (move.Degrees / _faceRotationTime));
                        }
                    })
                    .OnComplete(() => OnRotationComplete(checkCompletion));
        }
    }

    private void OnRotationComplete(bool checkCompletion)
    {
        if (checkCompletion && IsCubeCompleted())
        {
            HUDController.Instance.FinishGame();
        }
        else
            InputManager.BlockAllInput = false;
    }

    private bool IsCubeCompleted()
    {
        //two faces never share an exact list of cubies, so if the cubies are distributed in faces in the same
        //fashion as the beginning it means the cube is solved
        //Due to our representation, the 6 faces can be found with the planes at the borders of the instantiation
        foreach(var key in _facesInitialState.Keys)
        {
            //all the cubies that currently belong to the plane for this face
            var inPlane = _cubies.Where(c => Mathf.Abs(MoveInfo.s_axisPositionsRetrievers[key.Axis](c.transform) - key.AxisCoord) < 0.1f)
                                .ToList();

            //find if there is an initial face that contains this elements
            bool isPresent = _facesInitialState.Values.Any(f => f.All(inPlane.Contains));

            if (!isPresent)
                return false;
        }

        return true;
    }

    /// <summary>
    /// Get a random <see cref="MoveInfo"/>
    /// </summary>
    /// <param name="lastMove">Last move generated, so we make sure is not the reverse of this one</param>
    /// <returns></returns>
    private MoveInfo GetRandomMove(MoveInfo lastMove)
    {
        MoveInfo.Axis a = (MoveInfo.Axis)Random.Range(0, 3);
        float deg = Random.value > 0.5f ? c_rotationDegrees : -c_rotationDegrees;

        float coord = _validCoordinates[Random.Range(0, _validCoordinates.Count)];

        //to avoid undoing moves we simply change any value at random
        MoveInfo newMove = new MoveInfo(a, coord, deg);
        if (newMove.ReverseMove().Equals(lastMove))
            newMove.Degrees *= -1f;

        return newMove;
    }

    public void NotifySelection(Cubie c)
    {
        _selectedCubie = c;

        CameraController.Instance.BlockCamera = true;
    }

    /// <summary>
    /// Makes the <see cref="MoveInfo.ReverseMove"/> of the last move made
    /// </summary>
    public void UndoMove()
    {
        if (InputManager.BlockAllInput)
            return;

        if (_movesHistory.Count == 0)
            return;

        MoveInfo last = _movesHistory.Last();
        //take less time when undoing
        Rotate(last.ReverseMove(), _faceRotationTime/2f);

        _movesHistory.Remove(last);

        SaveHandler.SaveArray<MoveInfo>(c_historySavedData, _movesHistory.ToArray());
    }

    public static void RestartLevel()
    {
        //delete save but keep cube size
        SaveHandler.RestartGameData(s_sizeSelected);        
    }

    private void OnDestroy()
    {
        InputManager.OnSwipe -= TryToRotate;
    }
}
