﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cubie : MonoBehaviour
{
    private RubikHandler _owner;
    private int _id;

    public void Init(RubikHandler o, Vector3 initPos, int id)
    {
        _owner = o;
        _id = id;
        transform.position = initPos;

        name = $"Cubie {_id}";
    }

    private void OnMouseDown()
    {
        if(!InputManager.BlockAllInput)
            _owner.NotifySelection(this);
    }
}
