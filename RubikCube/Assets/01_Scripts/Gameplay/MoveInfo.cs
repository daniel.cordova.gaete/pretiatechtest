﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Representation of a Move
/// </summary>
[System.Serializable]
public class MoveInfo
{
    #region Helper Structures and Methods
    public enum Axis { X,Y,Z }

    /// <summary>
    /// Vectors for each axis
    /// </summary>
    public static readonly Dictionary<Axis, Vector3> c_axisVectors = new Dictionary<Axis, Vector3>
    {
        {Axis.X, Vector3.right },
        {Axis.Y, Vector3.up },
        {Axis.Z, Vector3.forward },
    };

    /// <summary>
    /// Dictionary to retrieve the coords of the planes for each axis
    /// </summary>
    public static readonly Dictionary<Axis, Func<Transform, float>> s_axisPositionsRetrievers = new Dictionary<Axis, Func<Transform, float>>
    {
        { Axis.X, (Transform t) => t.position.x },
        { Axis.Y, (Transform t) => t.position.y },
        { Axis.Z, (Transform t) => t.position.z }
    };

    /// <summary>
    /// Detect direction of movement and use it to determine rotation axis and direction
    /// </summary>
    /// <returns>The <see cref="MoveInfo"/> that represents the movement</returns>
    public static MoveInfo CalculateMove(Transform selectedCubie)
    {
        //we need to calculate the projection of the swipe (endMouse-startMouse) on the 3d world
        Vector3 objScreenPoint = Camera.main.WorldToScreenPoint(selectedCubie.position);
        Vector3 mouseWorldPoint = Camera.main.ScreenToWorldPoint(new Vector3(InputManager.MousePosition.x, InputManager.MousePosition.y, objScreenPoint.z));
        Vector3 offset = mouseWorldPoint - selectedCubie.position;

        //compare the world drag vector to the possible axis        
        Axis ax = GetRotationAxis(offset);
        float coord = s_axisPositionsRetrievers[ax](selectedCubie);
        float deg = RubikHandler.c_rotationDegrees * GetRotationDirection(offset, ax);

        return new MoveInfo(ax, coord, deg);
    }

    /// <summary>
    /// Gets the best suited rotation axis
    /// </summary>
    /// <param name="worldMovement">drag input projected into the world</param>
    /// <returns>Axis where the rotation whould happen</returns>
    private static Axis GetRotationAxis(Vector3 worldMovement)
    {
        //first find the axis closest to the camera.forward
        Axis cameraForward = FindMostParallelAxis(Camera.main.transform.forward);

        //now the axis for the rotation is the cross product between the drag and the forward axis
        Vector3 aproxAxis = Vector3.Cross(worldMovement, c_axisVectors[cameraForward]);

        return FindMostParallelAxis(aproxAxis);
    }

    /// <summary>
    /// Return the sign of the movement
    /// </summary>
    /// <param name="worldMovement">drag input projected into the world</param>
    /// <returns>The rotation sign based on the projection against the camera</returns>
    private static float GetRotationDirection(Vector3 worldMovement, Axis rotAxis)
    {
        //the "direction" of the rotation against the desired axis
        Vector3 rotDirection = Vector3.Cross(c_axisVectors[rotAxis], worldMovement);

        //the final results depends on the drag orientation regarding the camera
        float dot = Vector3.Dot(rotDirection, Camera.main.transform.forward);

        return Mathf.Sign(dot);
    }
    #endregion Helper Structures and Methods

    //The most parallel axis is the one with the "smallest" cross product
    private static Axis FindMostParallelAxis(Vector3 direction)
    {
        Axis axis = Axis.X;
        float magnitude = float.MaxValue;
        foreach (var t in c_axisVectors)
        {
            float newMag = Vector3.Cross(direction, t.Value).sqrMagnitude;
            if (newMag < magnitude)
            {
                axis = t.Key;
                magnitude = newMag;
            }
        }

        return axis;
    }

    /// <summary>
    /// Axis that is going to rotate
    /// </summary>
    public Axis SelectedAxis;
    /// <summary>
    /// "Row" position inside the axis that is going to rotate
    /// </summary>
    public float AxisCoord;
    /// <summary>
    /// Rotation in degrees
    /// </summary>
    public float Degrees;

    public MoveInfo(Axis sel, float coord, float deg = 90f)
    {
        SelectedAxis = sel;
        AxisCoord = coord;
        Degrees = deg;
    }

    /// <summary>
    /// Calculate if a Transform is close enough to the plane defined by this move
    /// </summary>
    /// <returns>true if the transform is close enough</returns>
    public Func<Transform, bool> TransformBelongsToSelectedPlane()
    {
        return t =>
        {
            float position = s_axisPositionsRetrievers[SelectedAxis](t);            

            return Mathf.Abs(position-AxisCoord) <= 0.1f;
        };
    }

    public MoveInfo ReverseMove()
    {
        return new MoveInfo(SelectedAxis, AxisCoord, -Degrees);
    }

    public Vector3 GetRotationAxis()
    {
        return c_axisVectors[SelectedAxis];
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;

        MoveInfo other = obj as MoveInfo;

        return other != null &&
                SelectedAxis == other.SelectedAxis &&
                AxisCoord == other.AxisCoord &&
                Degrees == other.Degrees;
    }
}
